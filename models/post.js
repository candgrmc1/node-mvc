const bcrypt = require('bcrypt')
var mongoose = require('mongoose');
var models = require('./load')
const saltRounds = 15;
module.exports = function(dbPath){
    dbPath.makeMongoConnection();
    
    var PostSchema = createSchema();
    var Post = mongoose.model('Post', PostSchema);
    
    
    var getPosts = function(){
        return Post.aggregate([
            {
                "$lookup": {
                    "from": "likes",
                    "localField": "_id",
                    "foreignField": "post_id",
                    "as": "likes",
                    
                }
            },
            {
                "$lookup": {
                    "from": "postcomments",
                    "localField": "_id",
                    "foreignField": "post",
                    "as": "comments",

                   
                }
            },
            {
                "$lookup": {
                    "from": "users",
                    "localField": "user_id",
                    "foreignField": "_id",
                    "as": "user",
                    
                }
            },
            {"$sort": {"created_at": -1}}
            
        ]).exec();
    }
    var followsPosts = (userID,callback)=>{
        models.FollowModel.getFollowIDS(userID,(follows)=>{
            followArr = follows.map(function(item){
                return item.user_id;
                
            })
            console.log(followArr)
            Post.where('user_id').in(followArr).populate('user_id').populate('comments').populate('likes').exec(function(err,data){
                console.log(data)
                callback(data)
            })
        })
    }
    var newPost = function(data,callback){
        Post.create({'user_id':data.user_id,'post': data.post ,'created_at': Date.now()},function(err, small){
            if (err) callback(false);
            callback(true)
        })
    }



    
    return {
        newPost: newPost,
        getPosts:getPosts,
        followsPosts:followsPosts
    }
}




function createSchema(){
    var Schema = mongoose.Schema; 
    return new Schema({
        user_id: {type: Schema.Types.ObjectId, ref: 'User' },
        post: { type: String, required: true },
        comments:[{type: Schema.Types.ObjectId, ref: 'PostComment'}],
        likes:[{type: Schema.Types.ObjectId, ref: 'Like'}],
        created_at:{type : Date, default: Date.now }
        },{ 
        versionKey: false 
            }
        );
}



