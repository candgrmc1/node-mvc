const mongoose = require('mongoose');


module.exports = function(dbPath){
    dbPath.makeMongoConnection();

    var CommentSchema = createSchema();
    var Comments = mongoose.model('PostComment', CommentSchema);
    var newComment = function(data){
        Comments.create({
            'post':data.post_id,
            'user':data.user_id,
            'username':data.username,
            'post_comment':data.post_comment,
            'created_at': Date.now()
        },(err,res) => {
            if (err) console.log(err);
            console.log(res)
            return true;
        })
    }

    return {
        newComment:newComment
    }
}


function createSchema(){
    var Schema = mongoose.Schema; 
    return new Schema({
        post: [{type: Schema.Types.ObjectId, ref: 'Post' }],
        user: [{type: Schema.Types.ObjectId, ref: 'User' }],
        username:{type:String,required:true},
        post_comment: {type:String,required:true},
        created_at:{type : Date, default: Date.now }

        }
        );
}