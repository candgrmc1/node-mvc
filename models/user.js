const bcrypt = require('bcrypt')
var mongoose = require('mongoose');

const saltRounds = 15;
module.exports = function(dbPath){
    dbPath.makeMongoConnection();
    
    var UserSchema = createSchema();
    var User = mongoose.model('User', UserSchema);
    
    


    var login = function(username,password,callback){
        User.findOne({'username':username},function(err,user){
            if(user){
                bcrypt.compare(password,user.password,function(err,res){
                 
                    if (err) callback(false);
                    if(res) callback(user);
                

                })
            }else{
                callback(false);
            }
            
        });

       
        
    }

    var getRandomUsers = (userID) => {
        return User.where('_id').ne(userID).limit(5);
    }

    var register = function(username,password){
        bcrypt.hash(password, saltRounds, function(err, hash) {
            User.create({'username':username,'password': hash},function(err, small){
                if (err) console.log(err);
                return true;
            })
          });
       
       
    }

    
    return {
        login:login,
        register:register, 
        User:User,
        getRandomUsers:getRandomUsers
    }
}




function createSchema(){
    var Schema = mongoose.Schema; 
    return new Schema({
        username: { type: String, required: true, unique:true },
        password: { type: String, required: true },
        },{ 
        versionKey: false 
            }
        );
}



