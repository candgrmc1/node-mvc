
var config = require('../config/config');


var dbPath = config.mongoPathProd
//var dbPath = config.mongoPathLocal



module.exports.UserModel = require('./user')(dbPath)
module.exports.PostModel = require('./post')(dbPath)
module.exports.FollowModel = require('./follow')(dbPath)
module.exports.CommentModel = require('./comment')(dbPath)
module.exports.LikeModel = require('./like')(dbPath)

