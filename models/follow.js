const bcrypt = require('bcrypt')
var mongoose = require('mongoose');

module.exports = function(dbPath){
    dbPath.makeMongoConnection()
    
    var FollowSchema = createSchema();
    var Follow = mongoose.model('Follow', FollowSchema);
    
    var getFollowers = function(userID){
        return Follow.where('user_id',userID).exec()
    }
    var getFollows = function(userID){
        return Follow.where('follower_id',userID).populate('user_id').populate('follower_id').exec()
    }
    var getFollowIDS = function(userID,callback){
        Follow.find({'follower_id':userID}).select('user_id').exec(function (err, data) {
            if (err) return next(err);
            callback(data)
        })
    }

    var follow = (user,toFollow) => {
        Follow.create({user_id:toFollow,follower_id:user},(err,res) => {
            if (err) console.log(err);
            return true;
        })
    }
    
    return {
        getFollowers:getFollowers,
        follow:follow,
        getFollows:getFollows,
        getFollowIDS:getFollowIDS
    }
}

function connect(dbPath){
    mongoose.connect(dbPath, function(err) {
        if (err) throw err;
      });
}


function createSchema(){
    var Schema = mongoose.Schema; 
    return new Schema({
        user_id: [{type: Schema.Types.ObjectId, ref: 'User' }],
        follower_id: [{type: Schema.Types.ObjectId, ref: 'User' }],
     
        }
        );
}



