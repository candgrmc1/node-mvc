const mongoose = require('mongoose')
String.prototype.capitalize = function(){
    return this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.makeMongoConnection = function(){
    mongoose.connect(this.toString(), function(err) {
        if (err) throw err;
      return this;

      });
}
