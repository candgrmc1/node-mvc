
module.exports = function(app,localStorage,router){
    
    var bodyParser = require('body-parser');
    const Controller = require('../controllers/load');
    
    app.use(bodyParser.urlencoded({'extended':'true'})); 
    app.use(bodyParser.json({ type: '*/*' }));

  

      app.use('/user', router, function (req, res,next) {
    const user = localStorage.getItem('session_user');

        if(!user){
            res.sendStatus(401)
        }else{
            next()
        }
      })
      
    app.get('/', (req, res) => {new Controller('IndexController',req,res).index()})
    app.get('/login',(req,res) =>{new Controller('IndexController',req,res).login()})
    app.post('/login',(req,res) => {new Controller('AuthController',req,res,localStorage).login()});
    app.get('/logout',(req,res) => {new Controller('AuthController',req,res,localStorage).login()});
    app.get('/register', (req,res) =>{new Controller('IndexController',req,res).register()})
    app.post('/register',(req,res) => {new Controller('AuthController',req,res,localStorage).register()});
    

    app.get('/user', (req,res) => {new Controller('UserController',req,res,localStorage).user()})
    app.get('/user/profile',(req,res)=>{new Controller('UserController',req,res,localStorage).profile()})
    app.get('/user/posts',(req,res)=>{new Controller('UserController',req,res,localStorage).posts()})
    app.get('/user/follows',(req,res)=>{new Controller('UserController',req,res,localStorage).friends()})

      
    app.post('/user/new_post',(req,res) => {new Controller('PostController',req,res,localStorage).newPost()})
    app.post('/user/new_comment/:post_id',(req,res) => {new Controller('PostController',req,res,localStorage).newComment()})
    app.get('/user/like_post/:post_id',(req,res) => {new Controller('PostController',req,res,localStorage).likePost()})
    app.get('/user/follow/:user',(req,res) => {new Controller('UserController',req,res,localStorage).follow()})
    app.get('/user/profile',(req,res) => new Controller('UserController',req,res,localStorage).profile())

}