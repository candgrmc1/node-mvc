const express = require('express')
var router = express.Router()
const app = express()
const port = process.env.PORT || 3000;
//const port = 3000;
var LocalStorage = require('node-localstorage').LocalStorage;
var localStorage = new LocalStorage('./storage');
require('./config/helper');
require('./routes/web')(app,localStorage,router)




app.use(express.static(__dirname + '/'));


app.listen(port, () => console.log(`Running on port ${port}!`))