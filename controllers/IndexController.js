module.exports = function(req,res){
    
    var index = function(){
        res.render('index.ejs')
    }
    var login =  function(){
        res.render('login.ejs')
    }

    var register = function(){
        res.render('register.ejs')
    }
    return {
        index:index,
        login:login,
        register:register
    }
}