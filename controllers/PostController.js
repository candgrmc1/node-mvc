
const models = require('../models/load');
module.exports = function(req,res,localStorage){
    
    if (typeof localStorage === "undefined" || localStorage === null) {
        var LocalStorage = require('node-localstorage').LocalStorage;
        localStorage = new LocalStorage('./storage');
      }
    const user_id = localStorage.getItem('user_id')

    var newPost = function(){
        var post = req.body.post;
     
;
        models.PostModel.newPost({user_id:user_id,post:post},(st)=>{
            res.redirect('/user')
        })
        
    }

    var newComment = function(){
        
        var post_id = req.params.post_id;
        ;
        var post_comment = req.body.post_comment;
        var username = JSON.parse(localStorage.getItem('session_user')).username
        models.CommentModel.newComment({
            post_id:post_id,
            user_id:user_id,
            username:username,
            post_comment:post_comment
        })
        res.redirect('/user')
    }

    var likePost = function(){
        var post_id = req.params.post_id;
        models.LikeModel.likePost({user_id:user_id,post_id:post_id},(suc)=>{
            if(suc){
                res.redirect('/user')
            }
        })
    }
    
    return {
        newPost:newPost,
        newComment:newComment,
        likePost:likePost   
    }
}