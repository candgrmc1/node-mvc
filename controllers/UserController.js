const models = require('../models/load');
const moment = require('moment');
module.exports = function(req,res){
    if (typeof localStorage === "undefined" || localStorage === null) {
        var LocalStorage = require('node-localstorage').LocalStorage;
        localStorage = new LocalStorage('./storage');
      }

const sideUsername = JSON.parse(localStorage.getItem('session_user')).username.capitalize()
      
const user_id = localStorage.getItem('user_id');
      
    var user = function(){
        var posts = [];
        var followers = [];
        var follows = []
        var randomUsers = [];
        
        
        Promise.all([
            models.PostModel.getPosts().then((p)=>{
                posts = p
            }),
            models.FollowModel.getFollowers(user_id).then((f)=>{
                followers = f
            }),
            models.FollowModel.getFollows(user_id).then((fw)=>{
                follows = fw
            }),
            models.UserModel.getRandomUsers(user_id).then((ru) => {
                randomUsers = ru
            }),
            

          ]).then(()=>{
            res.render('user/index.ejs',{
                posts:posts,
                followers:followers, 
                follows:follows,
                randomUsers:randomUsers,
                username:sideUsername,
                moment:moment
            })
              
          }).catch((err)=> {
            console.log(err)
        })
        
    }

    var follow = function(){
        var toFollow = req.params.user;
        console.log(toFollow)
        models.FollowModel.follow(user_id,toFollow)
        res.redirect('/user')

    }

    var profile = function(){
        res.render('user/profile.ejs')
    }

    var posts = function(){
        res.render('user/posts/posts.ejs')
    }

    var friends = function(){
        models.PostModel.followsPosts(localStorage.getItem('user_id'),(data)=>{

        res.render('user/friends.ejs',{
                posts:data,
                username:sideUsername,
                moment:moment
            })

        })
    }

    return {
        user:user,
        profile:profile,
        posts:posts,
        friends:friends,
        follow:follow
    }
}