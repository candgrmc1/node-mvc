
const models = require('../models/load');
module.exports = function(req,res,localStorage){
    if (typeof localStorage === "undefined" || localStorage === null) {
        var LocalStorage = require('node-localstorage').LocalStorage;
        localStorage = new LocalStorage('./storage');
      }
    var login = function(){
        var username = req.body.username;
        var password = req.body.password;
        
        models.UserModel.login(username,password,function(user){
            if(user){

                localStorage.setItem('session_user',JSON.stringify(user))
                console.log(user)
                localStorage.setItem('user_id',user._id)
                global.screenUsername = user.username
                res.redirect('/user')
            }else{
                res.redirect('/login')
            }
            
       })
    }

    var register = function(){
        var username = req.body.username;
        var password = req.body.password;
        models.UserModel.register(username,password)
        res.redirect('login')
    }

    var logout = function(){
        localStorage.removeItem('session_user')
        res.redirect('/login')
    }

    return {
        login:login,
        register:register,
        logout:logout
    }
}