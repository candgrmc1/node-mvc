var dir = '../controllers';
    var controller = function(controller,req,res){
        delete require.cache[require.resolve(dir+'/'+controller)];
         return require(dir+'/'+controller)(req,res);
    }
  
module.exports = controller
